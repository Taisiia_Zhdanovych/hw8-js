

// ## Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)

//  Це  модель документа, за якою  весь вміст сторінки представдяється 
//  у вигляді об'єктів, які можна змінювати.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?


// innerHTML - виводить вміст разом з тегами, включаючи текстову інформацію.
// innerText - виводить тільки текст.


// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
   
// Якщо треба звернутися до одного елемента, то найзручніший спосіб за Id.
// document.getElementById;
// є пошук за класом , за тегом, за атрибутом, пошук багатьох однотипних елементів одразу.
// getElementsByClassName;
// querySelector;
// querySelectorAll.
// Але найуніверсальніші це querySelector  та querySelectorAll - можливостей для пошуку більше.




// ## Завдання

// Код для завдань лежить в папці project.

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraph = document.querySelectorAll('p');

paragraph.forEach(el => {
  
    el.style.backgroundColor = '#ff0000';
    
});

console.log(paragraph);


// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const byId = document.getElementById('optionsList');
console.log(byId);
console.log(byId.parentElement);

if(byId.hasChildNodes()){
  console.log(byId.childNodes);
  for(let elem of byId.childNodes){
  console.log(elem.nodeName);
  console.log(elem.nodeType);
}
}
else{console.log('empty nodelist');};



// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>
 
  let newParagraf = document.createElement('p');
  newParagraf.classList='testParagraph';
  newParagraf.textContent='This is a paragraph';
// newParagraf.innerHTML = 'This is a paragraph' - ще один спосіб додати текст
 document.body.append(newParagraf)- //чи треба вставляти цей тег кудись?
  console.log(newParagraf);



// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів 
// присвоїти новий клас nav-item.
let headerMy= document.getElementsByClassName('main-header');
console.log(headerMy);
let liMy= document.querySelectorAll('header li');
console.log(liMy);
liMy.forEach(el =>{
     el.classList.add('nav-item');
});
console.log(liMy);




// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  

let elemSec = document.querySelectorAll('.section-title');
elemSec.forEach(el =>{
    el.classList.remove('section-title');
});

console.log(elemSec);

